<?php $__env->startSection('header'); ?>
    <h2>Blog List</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	 <?php echo Form::open(
		    array(
		        'route' => 'product.store', 
		        'class' => 'form', 
		        'novalidate' => 'novalidate', 
		        'files' => true)); ?>


		<div class="form-group">
		    <?php echo Form::label('Product Name'); ?>

		    <?php echo Form::text('name', null, array('placeholder'=>'Chess Board')); ?>

		</div>

		<div class="form-group">
		    <?php echo Form::label('Product SKU'); ?>

		    <?php echo Form::text('sku', null, array('placeholder'=>'1234')); ?>

		</div>

		<div class="form-group">
		    <?php echo Form::label('Product Image'); ?>

		    <?php echo Form::file('image', null); ?>

		</div>

		<div class="form-group">
		    <?php echo Form::submit('Create Product!'); ?>

		</div>
		<?php echo Form::close(); ?>

		</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>