<?php $__env->startSection('header'); ?>
    <h2>Upload Image</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<?php if(Session::has('message')): ?>
	<div class="alert alert-success">
		<?php echo Session('message'); ?>

	</div>
	<?php endif; ?>

     <?php echo Form::open(array('url'=>'product/upload','method'=>'POST', 'files'=>true, 'class'=>'form-horizontal')); ?>




		<div class="form-group">
		    <?php echo Form::label('image','Product Image', ['class'=>'control-label col-md-2']); ?>

			<div class="col-md-10">
		    <?php echo Form::file('file', null); ?>

			<span class="text-danger"><?php echo $errors->has('file')?$errors->first('file'):''; ?></span>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<?php echo Form::submit('Submit', ['class'=>'btn btn-primary']); ?>

			</div>
		</div>
		<?php echo Form::close(); ?>

		</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>