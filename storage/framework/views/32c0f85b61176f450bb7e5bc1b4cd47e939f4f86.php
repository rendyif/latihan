<?php $__env->startSection('header'); ?>
    <h2>Blog List</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <a href="blog/create" class="btn btn-primary">Add new</a>
    <table class="table table-bordered table-responsive" style="margin-top: 10px;">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Created at</th>
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <tr>
                <td><?php echo e($blog->id); ?></td>
                <td><?php echo e($blog->title); ?></td>
                <td><?php echo e($blog->description); ?></td>
                <td><?php echo e($blog->created_at); ?></td>
                <td>
                    <a href="<?php echo e(route('blog.edit', $blog->id)); ?>" class="btn btn-success">Edit</a>
                </td>
                <td>
                    <?php echo Form::open(['method'=>'delete', 'route'=>['blog.destroy', $blog->id]]); ?>

                    <?php echo Form::submit('Delete', ['class'=>'btn btn-danger', 'onclick'=>'return confirm("Do you want to delete this record?")']); ?>

                    <?php echo Form::close(); ?>

                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </tbody>
    </table>

    <?php echo e($blogs->links()); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>