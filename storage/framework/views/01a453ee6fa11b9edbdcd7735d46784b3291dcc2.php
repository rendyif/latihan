<DOCTYPE html>
<html lang="en">
<head>
    <title>Laravel 5.3 CRUD</title>
    <link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="page-header">
            <?php echo $__env->yieldContent('header'); ?>
        </div>
        <?php echo $__env->yieldContent('content'); ?>
    </div>
</body>
</html>