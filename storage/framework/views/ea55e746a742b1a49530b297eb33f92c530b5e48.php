<?php $__env->startSection('header'); ?>
    <h2>Edit Blog</h2>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo Form::model($blog, ['route'=>['blog.update', $blog->id], 'method'=>'PATCH', 'class'=>'form-horizontal']); ?>

        <div class="form-group">
            <?php echo Form::label('title', 'Title', ['class'=>'control-label col-md-2']); ?>

            <div class="col-md-10">
                <?php echo Form::text('title', null, ['class'=>'form-control']); ?>

                <?php echo $errors->has('title')?$errors->first('title'):''; ?>

            </div>
        </div>
        <div class="form-group">
            <?php echo Form::label('description', 'Description', ['class'=>'control-label col-md-2']); ?>

            <div class="col-md-10">
                <?php echo Form::textarea('description', null, ['class'=>'form-control']); ?>

                <?php echo $errors->has('description')?$errors->first('description'):''; ?>

            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <?php echo Form::submit('Save', ['class'=>'btn btn-primary']); ?>

            </div>
        </div>
    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>